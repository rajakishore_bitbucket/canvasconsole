﻿using CanvasConsole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanvasConsole.Commands.CommandInterface
{
    public interface ICommand
    {
        void CommandValidation(List<string> cmd);

        Canvas ExecuteCommand();
    }
}
