﻿using CanvasConsole.Commands.CommandInterface;
using CanvasConsole.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanvasConsole.Commands.ConcreteCreatorCommand
{
    public class QuitCommand : ICommand
    {
        public void CommandValidation(List<string> cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("missing command arguments");

            if (cmd.Any())
                throw new ArgumentException("should have no arguments");
        }

        public Canvas ExecuteCommand()
        {
            Environment.Exit(Environment.ExitCode);
            return null;
        }
    }
}
