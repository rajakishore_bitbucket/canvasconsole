﻿using System;
using System.Collections.Generic;
using System.Linq;
using CanvasConsole.Commands.FactoryCreatorCommand;
using CanvasConsole.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestCanvasConsole
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateCanvas()
        {
            Canvas canvas = null;
            List<string> input = new List<string>() { "C", "20", "4" };
            var command = CommandFactory.CreateCommandInstance(input, canvas);
            var param = input.Skip(1).ToList();
            command.CommandValidation(param);
            canvas = command.ExecuteCommand();

            Assert.AreEqual(20, canvas._width - 2);
            Assert.AreEqual(4, canvas._height - 2);
            Assert.IsNotNull(canvas.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NullParameterTest()
        {
            Canvas canvas = null;
            List<string> input = new List<string>() { "", "", "" };
            var command = CommandFactory.CreateCommandInstance(input, canvas);
            var param = input.Skip(1).ToList();
            command.CommandValidation(param);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CheckExecptionforNullCanvas()
        {
            Canvas canvas = null;
            List<string> input = new List<string>() { "L", "2", "2","2","1" };
            var command = CommandFactory.CreateCommandInstance(input, canvas);
            var param = input.Skip(1).ToList();
            command.CommandValidation(param);
        }


        
    }
}
