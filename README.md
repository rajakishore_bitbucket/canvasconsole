# CanvasConsole

### Given Task:
You're given the task of writing a simple console version of a drawing program. In a nutshell, the program should work as follows:

- create a new canvas
- start drawing on the canvas by issuing various commands
- quit

> At the moment, the program should support the following commands:

```sh
C w h   Should create a new canvas of width w and height h.
```
```sh
 L x1 y1 x2 y2  Should create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are - supported. Horizontal and vertical lines will be drawn using the 'x' character.
```
```sh
 R x1 y1 x2 y2  Should create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.
```
```sh
 B x y c Should  fill the entire area connected to (x,y) with "colour" c. The behaviour of this is the same as that of the "bucket fill" tool in paint programs.
```
```sh
Q Should quit the program.
```
### UML Class Diagram

 ![UML Class Diagram](https://bitbucket.org/rajakishore_bitbucket/canvasconsole/raw/915905e2e884a4bc2a781d83c05fbfba9f21a306/UML_Class_Diagram.png) 